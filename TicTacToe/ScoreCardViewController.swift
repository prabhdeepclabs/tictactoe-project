//
//  ScoreCardViewController.swift
//  TicTacToe
//
//  Created by Bhasker on 2/9/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

class ScoreCardViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var tasksTable: UITableView!
    
    var scorecardArray:[String] = [] // array for updating the table from persistent storage
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scorecardArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel!.text = scorecardArray[indexPath.row]
        return cell
    }
    
    // load scorecardArray in reverse from persistent storage and refresh table to display latest result on top
    override func viewWillAppear(animated: Bool) {
        if var storedDetails : AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("scorecard") {
            for score in stride(from : storedDetails.count-1, to : -1, by : -1) {
                scorecardArray.append(storedDetails[score] as NSString)
            }
        }
        tasksTable.reloadData()
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // to delete a row i.e a to do item from table and update the permanent storage as well
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            scorecardArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            let fixedDetails = scorecardArray
            NSUserDefaults.standardUserDefaults().setObject(fixedDetails, forKey: "scorecard")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    // reset the scorecard i.e table
    @IBAction func resetButton(sender: AnyObject) {
        scorecardArray = []
        let fixedDetails = scorecardArray
        NSUserDefaults.standardUserDefaults().setObject(fixedDetails, forKey: "scorecard")
        NSUserDefaults.standardUserDefaults().synchronize()
        tasksTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
