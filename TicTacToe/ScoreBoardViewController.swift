//
//  ScoreBoardViewController.swift
//  TicTacToe
//
//  Created by Bhasker on 2/5/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

// current game scoreboard
class ScoreBoardViewController: UIViewController {

    @IBOutlet weak var playerOneName: UILabel!
    @IBOutlet weak var playerOneWon: UILabel!
    @IBOutlet weak var playerOneLost: UILabel!
    
    @IBOutlet weak var playerTwoName: UILabel!
    @IBOutlet weak var playerTwoWon: UILabel!
    @IBOutlet weak var playerTwoLost: UILabel!
    
    @IBOutlet weak var gamesTied: UILabel!
    @IBOutlet weak var gamesPlayed: UILabel!
    
    @IBAction func resetScore(sender: AnyObject) {
        PlayerOneWonGlobal = 0
        PlayerOneLostGlobal = 0
        gamesTiedGlobal = 0
        gamesPlayedGlobal = 0
        updateLabels() // updating all labels
    }
    
    // updating all the labels
    func updateLabels() {
        playerOneName.text = "\(playerOneNameGlobal)"
        playerOneWon.text = "\(PlayerOneWonGlobal)"
        playerOneLost.text = "\(PlayerOneLostGlobal)"
        
        playerTwoName.text = "\(playerTwoNameGlobal)"
        playerTwoWon.text = "\(PlayerOneLostGlobal)"
        playerTwoLost.text = "\(PlayerOneWonGlobal)"
        
        gamesTied.text = "\(gamesTiedGlobal)"
        gamesPlayed.text = "\(gamesPlayedGlobal)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
