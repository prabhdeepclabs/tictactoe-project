//
//  DetailsViewController.swift
//  TicTacToe
//
//  Created by Bhasker on 2/4/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

// global variables for use in scoreboard viewcontroller to display names of players
var playerOneNameGlobal = "Player 1"
var playerTwoNameGlobal = "Player 2"
// global variables for use in boardgame view controller to use those cross and nought icons in game
var playerOneChosenIcon = UIImage(named: "Nought_skin_1")
var playerTwoChosenIcon = UIImage(named: "Cross_skin_1")

class MainScreenViewController: UIViewController, UITextFieldDelegate {
    // text fields for names of players
    @IBOutlet weak var playerOneName: UITextField!
    @IBOutlet weak var playerTwoName: UITextField!
    // image views for storing the selected icon by players and updating the icon of buttons
    var imageForNought = UIImage(named: "Nought_skin_1")
    var imageForCross = UIImage(named: "Cross_skin_1")
    // buttons for refering to the icon buttons of cross and nought
    @IBOutlet weak var playerOneNoughtButton: UIButton!
    @IBOutlet weak var playerOneCrossButton: UIButton!
    @IBOutlet weak var playerTwoNoughtButton: UIButton!
    @IBOutlet weak var playerTwoCrossButton: UIButton!
    
    // a cross or nought is selected by a player so change background color of icon button
    @IBAction func iconSelectPressed(sender: AnyObject) {
        /* tags used - structure wrt storyboard  9  10
                                                12  11  */
        for tag in 9...12 {
            var button : UIButton = view.viewWithTag(tag) as UIButton
            if (sender.tag%2 == 0) {
                if (tag%2 == 0) {
                    button.backgroundColor = UIColor.lightGrayColor()
                } else {
                    button.backgroundColor = UIColor.clearColor()
                }
            } else {
                if (tag%2 != 0) {
                    button.backgroundColor = UIColor.lightGrayColor()
                } else {
                    button.backgroundColor = UIColor.clearColor()
                }
            }
        }
    }
    
    // change style of nought icon buttons based on image of button pressed
    @IBAction func changeStyleOfNought(sender: AnyObject) {
        // tags used - structure wrt storyboard - tags 1 2 3 4
        // have look at the names of the images of skins of noughts and crosses
        
        playerOneNoughtButton.setImage(UIImage(named: "Nought_skin_\(sender.tag)"), forState: .Normal)
        playerTwoNoughtButton.setImage(UIImage(named: "Nought_skin_\(sender.tag)"), forState: .Normal)
        imageForNought = UIImage(named: "Nought_skin_\(sender.tag)")!
    }
    
    // change style of cross icon buttons based on image of button pressed
    @IBAction func changeStyleOfCross(sender: AnyObject) {
        // tags used - structure wrt storyboard - tags 5 6 7 8
        // have look at the names of the images of skins of noughts and crosses
        
        playerOneCrossButton.setImage(UIImage(named: "Cross_skin_\(sender.tag - 4)"), forState: .Normal)
        playerTwoCrossButton.setImage(UIImage(named: "Cross_skin_\(sender.tag - 4)"), forState: .Normal)
        imageForCross = UIImage(named:"Cross_skin_\(sender.tag - 4)")!
    }
    
    // update the global variabls for player names and nought cross icons on start game button press and segue to Boardgame view controller
    @IBAction func startGame(sender: AnyObject) {
        if(playerOneNoughtButton.backgroundColor != UIColor.clearColor()) {
            playerOneChosenIcon = imageForNought
            playerTwoChosenIcon = imageForCross
        } else {
            playerOneChosenIcon = imageForCross
            playerTwoChosenIcon = imageForNought
        }
        println(playerOneChosenIcon)
        println(playerTwoChosenIcon)
        playerOneNameGlobal = playerOneName.text
        playerTwoNameGlobal = playerTwoName.text
        
        // Clearing the current scoreboard for new game by the nulling the global scores
        PlayerOneWonGlobal = 0
        PlayerOneLostGlobal = 0
        gamesTiedGlobal = 0
        gamesPlayedGlobal = 0
        
        self.view.endEditing(true)
    }
    
    // to fetch score details from persistent storage in an array and append the current score to it and store it and segue to scorecard view controller to display table
    @IBAction func scorecardShow(sender: AnyObject) {
        var scorecardArray : [String] = []
        if var storedDetails : AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("scorecard") {
            for score in storedDetails as NSArray {
                scorecardArray.append(score as NSString)
            }
        }
        
        if gamesPlayedGlobal != 0 {
            // append currrent score only if played game
            
            var scoreDetailCurrent = "\(playerOneNameGlobal) vs \(playerTwoNameGlobal)\t\t\(PlayerOneWonGlobal)-\(PlayerOneLostGlobal)\t\t\(gamesTiedGlobal)\t\(gamesPlayedGlobal)"
            scorecardArray.append(scoreDetailCurrent)
            let fixedDetails = scorecardArray
            NSUserDefaults.standardUserDefaults().setObject(fixedDetails, forKey: "scorecard")
            NSUserDefaults.standardUserDefaults().synchronize()
            gamesPlayedGlobal = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerOneName.delegate = self
        playerTwoName.delegate = self
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
