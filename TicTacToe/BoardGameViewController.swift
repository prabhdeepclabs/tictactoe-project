//
//  ViewController.swift
//  TicTacToe
//
//  Created by Bhasker on 2/2/15.
//  Copyright (c) 2015 Bhasker. All rights reserved.
//

import UIKit

var PlayerOneWonGlobal = 0
var PlayerOneLostGlobal = 0
var gamesTiedGlobal = 0
var gamesPlayedGlobal = 0

class BoardGameViewController: UIViewController {
    
    var isplayerOne = true // keeps track of player one's turn and player one gets Noughts
    var gameStateArray = [0, 0, 0, 0, 0, 0, 0, 0, 0] // 0 = empty, 1 = Nought, 2 = cross in that cell
    let winningCombinationArray = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]
    var winner = 0 // 0 for no winnner , 1 for Nought has won , 2 for cross has won
    var goNumber = 1 // to keep track of last turn if no one has won declare it as draw
    
    @IBOutlet weak var resultLabel: UILabel! // Label for displaying the result
    
    @IBOutlet weak var playAgainButton: UIButton! // outlet to create animation of play again button
    @IBOutlet weak var scoreButton: UIButton! // outlet to create animation of score button
    
    // buttons used for playing turn i.e. to place a Nought or cross at that cell on the board
    @IBAction func buttonPressed(sender: AnyObject) {
        // respond only if button pressed first time and no one has won
        if (gameStateArray[sender.tag] == 0 && winner == 0) {
            var image = UIImage()
            if (isplayerOne) {
                image = playerOneChosenIcon!
                gameStateArray[sender.tag] = 1
                isplayerOne = false
            } else {
                image = playerTwoChosenIcon!
                gameStateArray[sender.tag] = 2
                isplayerOne = true
            }
            sender.setImage(image, forState: .Normal)
            
            for combination in winningCombinationArray {
                if(gameStateArray[combination[0]] == gameStateArray[combination[1]] && gameStateArray[combination[1]] == gameStateArray[combination[2]] && gameStateArray[combination[0]] != 0) {
                    winner = gameStateArray[combination[0]]
                    println("winner found")
                }
            }
            
            if (winner != 0 || goNumber == 9) {
                gamesPlayedGlobal++
                if (winner == 1) {
                    resultLabel.text = playerOneNameGlobal+" has won"
                    PlayerOneWonGlobal++
                } else if (winner == 2)  {
                    resultLabel.text = playerTwoNameGlobal+" has won"
                    PlayerOneLostGlobal++
                } else {
                    resultLabel.text = "Its a draw"
                    gamesTiedGlobal++
                }
                UIView.animateWithDuration(0.5, animations: {
                    self.resultLabel.hidden = false
                    self.resultLabel.center = CGPointMake(self.resultLabel.center.x + 400, self.resultLabel.center.y)
                    self.playAgainButton.alpha = 1
                    self.scoreButton.alpha = 1
                })
                println(self.resultLabel.center)
            }
            goNumber++
        }
    }
    
    // resetting the game
    @IBAction func playAgainButton(sender: AnyObject) {
        isplayerOne = true
        gameStateArray = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        winner = 0
        goNumber = 1
        resultLabel.hidden = true
        resultLabel.center = CGPointMake(resultLabel.center.x - 400 , resultLabel.center.y)
        playAgainButton.alpha = 0
        scoreButton.alpha = 0
        
        for cell in 0..<9 {
            //  clearing images of buttons
            var button : UIButton = view.viewWithTag(cell) as UIButton
            button.setImage(nil, forState: .Normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        resultLabel.center = CGPointMake(resultLabel.center.x - 400 , resultLabel.center.y)
        resultLabel.hidden = true
        playAgainButton.alpha = 0
        scoreButton.alpha = 0
    }
}

